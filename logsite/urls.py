"""
* URL Configuration for Logsite
* Maps URLs to views
* Jessica Gaines 09-17
"""
from django.conf.urls import url
from django.contrib import admin
from logsite.views import entry_type, daily, weekly, monthly, yearly, all, main
from newapp.views import add_MileageEntry, add_RaceEntry, add_IntervalEntry, add_Repeats, thanks

urlpatterns = [
    url(r'^admin/', admin.site.urls),
	url(r'^entry/$', entry_type),
	url(r'^entry/mileage/$', add_MileageEntry),
	url(r'^entry/race/$', add_RaceEntry),
	url(r'^entry/intervals/$', add_IntervalEntry),
	url(r'^entry/intervals/(\d{1,2})/$', add_Repeats),
	url(r'^entry/thanks/$', thanks),
	url(r'^main/$', main),
	url(r'^main/daily/$', daily),
	url(r'^main/weekly/$', weekly), 
	url(r'^main/monthly/$', monthly), 
	url(r'^main/yearly/$', yearly),
	url(r'^main/lifetime/$', all),
]
