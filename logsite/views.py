"""
* View functions take an HTTP request and return an HTTP Response
* Functions associated with displaying web pages
* Jessica Gaines 09-17
"""

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
import datetime
from datetime import timedelta
from newapp.models import MileageEntry, IntervalEntry, RaceEntry, Entry
from django.db.models import Min

# Contains a set of all entries between two dates
# Arguments: mindate and maxdate -- the inclusive date range of the entries
# Fields: earliest_date -- the minimum date in the range
#			latest_date -- the maximum date in the range
#			entries -- the list of all entries in the date range
#			num_runs -- the number of entries in the date range
#			total -- the total mileage run in the date range
class EntrySet:
	def __init__(self, mindate, maxdate):
		if(mindate > maxdate):
			raise ValueError('Dates out of order in EntrySet.')
		self.earliest_date = mindate
		self.latest_date = maxdate
		self.entries = Entry.objects.filter(date__range=[mindate,maxdate]).order_by('date','id')
		self.num_runs = self.entries.count()
		self.total = self.get_total_mileage()
	def get_total_mileage(self):
		total = 0
		for entry in self.entries:
			total = total + entry.total_distance
		return total

# Directs the url associated with entry_type to the correct webpage	
def entry_type(request):
	return render_to_response('entry_type.html',locals())

# Redirects to the main page with daily stats	
def main(request):
	return HttpResponseRedirect('daily/')

# Finds the current date and passes all entries from today to the main page displaying daily stats	
def daily(request):
	today = datetime.date.today()
	entries = Entry.objects.filter(date=today)
	min = Entry.objects.all().aggregate(Min('date'))
	earliest_date = min.get('date__min')
	working_date = today
	sets = []
	streak_days = 0
	end_streak = False
	while working_date >= earliest_date:
		es = EntrySet(working_date, working_date)
		if es.total > 0:
			sets.append(es)
			if end_streak == False: streak_days+=1
		else:
			end_streak = True
		working_date = working_date-timedelta(days=1)
	if streak_days < 0: streak_days=0
	return render_to_response('daily.html', locals())

# Finds the current date and passes all entries from today and the previous six days to the 
#	main page displaying weekly stats
def weekly(request):
	min = Entry.objects.all().aggregate(Min('date'))
	earliest_date = min.get('date__min')
	today = datetime.date.today()
	working_date = today
	weekday = working_date.weekday()
	while(working_date.weekday() != 0): # Start weeks on Monday
		working_date=working_date-timedelta(days=1)
	entries = Entry.objects.filter(date__range = [working_date,today])
	this_week = EntrySet(working_date, today)
	sets = []
	while working_date >= earliest_date:
		es = EntrySet(working_date, working_date+timedelta(days=6))
		if es.total > 0:
			sets.append(es)
		working_date = working_date-timedelta(days=7)
	return render_to_response('weekly.html', locals())

# Finds the current month and passes all entries from this month to the main page displaying 
#	monthly stats
def monthly(request):
	today = datetime.date.today()
	min = Entry.objects.all().aggregate(Min('date'))
	earliest_date = min.get('date__min')
	working_date = today + timedelta(days=1)
	sets = []
	while working_date >= earliest_date:
		if(working_date.day != 1):
			next_date = datetime.date(working_date.year,working_date.month,1)
		else:
			if working_date.month == 1:
				next_year = working_date.year - 1
				next_month = 12
			else:
				next_year = working_date.year
				next_month = working_date.month - 1
			next_date = datetime.date(next_year,next_month,1)
		es = EntrySet(next_date, working_date-timedelta(days=1))
		if es.total > 0:
			sets.append(es)
		working_date = next_date
	return render_to_response('monthly.html', locals())

# Finds the current year and passes all entries from this year to the main page displaying 
#	yearly stats
def yearly(request):
	today = datetime.date.today()
	year = today.year
	min = Entry.objects.all().aggregate(Min('date'))
	earliest_date = min.get('date__min')
	working_date = today + timedelta(days=1)
	sets = []
	while working_date >= earliest_date:
		if working_date.month == 1 and working_date.day == 1:
			next_year = working_date.year - 1
		else:
			next_year = working_date.year
		next_date = datetime.date(next_year,1,1)
		es = EntrySet(next_date, working_date-timedelta(days=1))
		if es.total > 0:
			sets.append(es)
		working_date = next_date
	races = RaceEntry.objects.filter(date__year=year).order_by('race_distance')
	best_races = get_best_races(races)
	return render_to_response('yearly.html', locals())
	
# Passes all entries to the main page displaying lifetime stats
def all(request):
	today = datetime.date.today()
	min = Entry.objects.all().aggregate(Min('date'))
	earliest_date = min.get('date__min')
	es = EntrySet(earliest_date,today)
	sets = [es]
	races = RaceEntry.objects.all().order_by('race_distance')
	best_races = get_best_races(races)
	return render_to_response('all.html', locals())

# Returns a dictionary with the races with the lowest times for each distance
# Argument: races, a set of RaceEntry objects
# Return: a dictionary that maps race distance to the RaceEntry with the lowest time
def get_best_races(races):
	best_races = dict()
	for race in races:
		if not (race.race_distance) in best_races:
			best_races[race.race_distance]=race
		elif race.time < best_races[race.race_distance].time:
			best_races[race.race_distance]=race
	return best_races