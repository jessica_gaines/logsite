"""
* View functions take an HTTP request and return an HTTP Response
* Functions associated with displaying forms for user input
* Functions validate user input and then save the input to the database
* Jessica Gaines 09-17
"""
from django.shortcuts import render
from newapp.models import Entry, MileageEntry, RaceEntry, IntervalEntry, Repeat
from newapp.forms import MileageEntryForm, RaceEntryForm, IntervalEntryForm, RepeatForm
from django.http import HttpResponseRedirect

# Displays the MileageEntryForm and then saves results as a MileageEntry
def add_MileageEntry(request):
	if request.method == 'POST':
		form = MileageEntryForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/entry/thanks/')
	else:
		form = MileageEntryForm()
	return render(request, 'mileage.html', {'form': form})

# Displays the IntervalEntryForm and then saves results as an IntervalEntry	
def add_IntervalEntry(request):
	if request.method == 'POST':
		form = IntervalEntryForm(request.POST)
		if form.is_valid():
			form.save()
			n = request.POST.get('number_of_repeats')
			return HttpResponseRedirect('/entry/intervals/%s' % n)		
	else:
		form = IntervalEntryForm()
	return render(request, 'intervals.html', {'form': form})

# Takes the number of repeats as an input and displays the correct number of 
#	RepeatForms in a single form.  Saves all repeats to the database.
def add_Repeats(request, n):
	if request.method == 'POST':
		form = RepeatForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/entry/thanks')		
	else:
		form = RepeatForm()
	return render(request, 'repeats.html', {'form': form, 'id':id, 'n':range(int(n))})
	
# Displays the RaceEntryForm and then saves the results as a RaceEntry			
def add_RaceEntry(request):
	if request.method == 'POST':
		form = RaceEntryForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/entry/thanks/')
	else:
		form = RaceEntryForm()
	return render(request, 'race.html', {'form': form})

# After input has been saved to the database, redirect to prevent form resubmission	
def thanks(request):
	return HttpResponseRedirect('/main/daily/')