"""
* Forms used by newapp to accept user input about running workouts
* Features an inheritance hierarchy for different types of log entries
* Jessica Gaines 09-17
"""

from django import forms
from newapp.models import Entry, MileageEntry, IntervalEntry, RaceEntry, Repeat
from django.forms import ModelForm

# Superclass -- all forms require input of date and total distance
# Comments are included with each subclass so that they are found at the end of the form
class EntryForm(ModelForm):
	class Meta:
		model = Entry
		fields = ['date','total_distance']

# MileageEntryForm extends the general EntryForm with input fields for the total time and 
# 	whether or not the run was flagged as a long run		
class MileageEntryForm(EntryForm):
	class Meta:
		model = MileageEntry
		fields = EntryForm.Meta.fields + ['total_time','long_run','comments']

# RaceEntryForm extends the general EntryForm with input fields for the race distance, race time, and course
class RaceEntryForm(EntryForm):
	class Meta:
		model = RaceEntry
		fields = EntryForm.Meta.fields + ['race_distance', 'time', 'course', 'comments']

# IntervalEntryForm extends the general EntryForm with an input field for the number of repeats associated with the workout
class IntervalEntryForm(EntryForm):
	class Meta:
		model = IntervalEntry
		fields = EntryForm.Meta.fields + ['number_of_repeats','comments']

# A form to input individual repeats
# Automatically associates the repeat with the most recent interval workout
# Includes input fields for the distance and duration of the repeat		
class RepeatForm(ModelForm):
	class Meta:
		model = Repeat
		fields = ['distance','time']