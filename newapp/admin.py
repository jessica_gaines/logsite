from django.contrib import admin
from .models import MileageEntry
from .models import IntervalEntry
from .models import RaceEntry
from .models import Repeat

admin.site.register(MileageEntry)
admin.site.register(IntervalEntry)
admin.site.register(RaceEntry)
admin.site.register(Repeat)

