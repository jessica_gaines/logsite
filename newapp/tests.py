"""
* Test suite for the database used by newapp to store and access running log data
* Jessica Gaines 09-17
"""

from django.test import TestCase
from .models import Entry, MileageEntry, RaceEntry, Repeat, IntervalEntry
from datetime import timedelta

class EntryModelTest(TestCase):
	def setUp(self):
		e = Entry(date="2017-09-07", comments="",total_distance=5)
		e.save()
	def test_string_representation(self):
		e = Entry.objects.get(id=1)
		self.assertEqual(str(e), "5.0 miles on 2017-09-07")
		e.total_distance = 1
		e.save()
		e = Entry.objects.get(id=1)
		self.assertEqual(str(e), "1.0 mile on 2017-09-07")

class MileageEntryModelTest(TestCase):
	def setUp(self):
		m = MileageEntry(date="2017-09-04", 
							total_distance = 8, 
							total_time=timedelta(minutes=60), 
							long_run=False)
		m.save()
	def test_string_representation(self):
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "8.0 miles in 1:00:00 on 2017-09-04")
		m.date = "2017-09-05"
		m.save()
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "8.0 miles in 1:00:00 on 2017-09-05")
		m.total_distance = 8.5
		m.save()
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "8.5 miles in 1:00:00 on 2017-09-05")
		m.total_time = timedelta(minutes=62,seconds=13)
		m.save()
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "8.5 miles in 1:02:13 on 2017-09-05")
		m.total_distance = 1
		m.save()
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "1.0 mile in 1:02:13 on 2017-09-05")
		m.long_run = True
		m.total_distance = 8.5
		m.save()
		m = MileageEntry.objects.get(id=1)
		self.assertEqual(str(m), "8.5 mile long run in 1:02:13 on 2017-09-05")
	def test_comments(self):
		m=MileageEntry.objects.get(id=1)
		self.assertEqual(m.comments, "")
		m.comments = "sample comments"
		m.save()
		self.assertEqual(m.comments, "sample comments")
		
class RaceEntryModelTest(TestCase):
	def setUp(self):
		r = RaceEntry(date="2017-04-30", 
						comments="Platteville", 
						total_distance=6, 
						race_distance=3.1, 
						time=timedelta(minutes=17,seconds=55),
						course="track")
		r.save()
	def test_string_representation(self):
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(str(r), "Race on 2017-04-30: 3.1 miles in 0:17:55")
		r.date = "2017-04-29"
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(str(r), "Race on 2017-04-29: 3.1 miles in 0:17:55")
		r.race_distance = 6.2
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(str(r), "Race on 2017-04-29: 6.2 miles in 0:17:55")
		r.time = timedelta(minutes=37, seconds=30)
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(str(r), "Race on 2017-04-29: 6.2 miles in 0:37:30")
		r.race_distance = 1
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(str(r), "Race on 2017-04-29: 1.0 mile in 0:37:30")
	def test_comments(self):
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(r.comments, "Platteville")
		r.comments = "Platteville meet"
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(r.comments, "Platteville meet")
	def test_total_distance(self):
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(r.total_distance, 6)
		r.total_distance = 10
		r.save()
		r = RaceEntry.objects.get(id=1)
		self.assertEqual(r.total_distance, 10)
		
class RepeatModelTest(TestCase):
	def setUp(self):
		interval_entry = IntervalEntry(date="2017-09-05", comments="speed sessions", total_distance=8)
		interval_entry.save()
		rep = Repeat(entry_id=interval_entry.id,distance = 0.5, time = timedelta(minutes=2,seconds=50))
		rep.save()
	def test_string_representation(self):
		rep = Repeat.objects.get(id=1)
		self.assertEqual(str(rep), "0.5 miles in 0:02:50")
		rep.distance = 1
		rep.save()
		rep = Repeat.objects.get(id=1)
		self.assertEqual(str(rep), "1.0 mile in 0:02:50")
		
class IntervalEntryModelTest(TestCase):
	def setUp(self):
		interval_entry = IntervalEntry(date="2017-09-05", comments="speed sessions", total_distance=8)
		interval_entry.save()
		rep1 = Repeat(entry_id=interval_entry.id,distance = 0.5, time = timedelta(minutes=2,seconds=50))
		rep1.save()
		rep2 = Repeat(entry_id=interval_entry.id, distance = 0.5, time = timedelta(minutes=2, seconds=49))
		rep2.save()
		interval_entry2 = IntervalEntry(date="2017-09-07", comments="workout 2", total_distance=8)
		interval_entry2.save()
	def test_string_representation(self):
		interval_entry = IntervalEntry.objects.get(id=1)
		interval_entry2 = IntervalEntry.objects.get(id=2)
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-05:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\nTotal: 8.0 miles")
		rep3 = Repeat(entry_id=interval_entry.id,distance = 0.5, time = timedelta(minutes=2,seconds=50))
		rep3.save()
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-05:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\n0.5 miles in 0:02:50\nTotal: 8.0 miles")
		rep4 = Repeat(entry_id=interval_entry2.id,distance = 0.5, time = timedelta(minutes=2,seconds=50))
		rep4.save()
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-05:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\n0.5 miles in 0:02:50\nTotal: 8.0 miles")
		interval_entry.date = "2017-09-03"
		interval_entry.save()
		interval_entry = IntervalEntry.objects.get(id=1)
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-03:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\n0.5 miles in 0:02:50\nTotal: 8.0 miles")
		interval_entry.total_distance = 10
		interval_entry.save()
		interval_entry = IntervalEntry.objects.get(id=1)
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-03:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\n0.5 miles in 0:02:50\nTotal: 10.0 miles")
		interval_entry.total_distance = 1
		interval_entry.save()
		interval_entry = IntervalEntry.objects.get(id=1)
		self.assertEqual(str(interval_entry), "Intervals on 2017-09-03:\n0.5 miles in 0:02:50\n0.5 miles in 0:02:49\n0.5 miles in 0:02:50\nTotal: 1.0 mile")
	def test_comments(self):
		interval_entry = IntervalEntry.objects.get(id=1)
		interval_entry2 = IntervalEntry.objects.get(id=2)
		self.assertEqual(interval_entry.comments, "speed sessions")
		self.assertEqual(interval_entry2.comments, "workout 2")





