# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-07 16:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('newapp', '0005_auto_20170907_1256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='intervalentry',
            name='iid',
        ),
        migrations.AlterField(
            model_name='intervalentry',
            name='entry_ptr',
            field=models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='newapp.Entry'),
        ),
    ]
