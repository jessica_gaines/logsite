""" 
* Models used by newapp, a running log app that tracks running workouts
* Inheritance hierarchy of types of log entries
* Jessica Gaines 09-17
"""

from django.db import models

# Concrete superclass for all running log entries
# Fields: date -- the date of the run
#		total_distance -- total distance including any warm-up, cool-down, repeats, recovery jog, race, and/or mileage
#		comments -- any additional comments about the run
class Entry(models.Model):
	date = models.DateField()
	comments = models.TextField()
	total_distance = models.FloatField()
	def __str__(self):
		rtnstring = "%s miles on %s" % (str(self.total_distance), str(self.date))
		if(self.total_distance == 1):
			rtnstring = rtnstring.replace("miles","mile")
		return rtnstring
		
# MileageEntry class inherits from the Entry superclass
# Used to log simple runs
# Fields: total_time -- the duration of the run
#			long_run -- boolean expressing whether or not the run should be flagged as a long run
class MileageEntry(Entry):
	total_time = models.DurationField()
	long_run = models.BooleanField()
	def __str__(self):
		rtnstring = "%s miles in %s on %s" % (str(self.total_distance), str(self.total_time), str(self.date))
		if(self.long_run):
			rtnstring = rtnstring.replace("miles","mile long run")
		elif(self.total_distance == 1):
			rtnstring = rtnstring.replace("miles","mile")
		return rtnstring
		
# RaceEntry class inherits from the Entry superclass
# Used to log races
# Fields: race_distance -- races will be categorized by distance to determine personal records and track progress
# 			time -- time achieved in the race
#			course -- races will be categorized by course to compare times on the same course
class RaceEntry(Entry):
	race_distance = models.FloatField()
	time = models.DurationField()
	course = models.CharField(max_length = 30)
	def __str__(self):
		rtnstring = "Race on %s: %s miles in %s" % (str(self.date), str(self.race_distance), str(self.time))
		if(self.race_distance == 1):
			rtnstring = rtnstring.replace("miles","mile")
		return rtnstring

# IntervalEntry class inherits from the Entry superclass
# Used to log runs that are broken into segments of speed and recovery
# Fields: number_of_repeats -- used to provide a form with the correct number of blank spaces for repeats
class IntervalEntry(Entry):
	number_of_repeats = models.PositiveSmallIntegerField(default=0)
	def __str__(self):
		rtnstring = "Intervals on %s:" % (str(self.date))
		s = "\n"
		for interval in Repeat.objects.filter(entry_id=self.id):
			rtnstring = s.join((rtnstring,str(interval)))
		footer = "Total: %s miles" % (str(self.total_distance))
		if(self.total_distance == 1):
			footer = footer.replace("miles","mile")
		rtnstring = s.join((rtnstring,footer))
		return rtnstring

# Repeat class is tied to the IntervalEntry class -- multiple repeats are associated with an interval workout
# Fields: entry_id -- the integer id of the IntervalEntry associated with the repeat.  A ManyToOneField for 
# 				that held an IntervalEntry object was considered, but in order to omit this field from the 
#				RepeatForm and associate repeats with the most recent IntervalEntry, a default IntervalEntry
#				was needed.  A Repeat database table that depends on the creation of an IntervalEntry object 
#				cannot be created at the same time as the IntervalEntry database table.
#			distance -- distance for a single repeat
#			time -- split time for the repeat
class Repeat(models.Model):
	entry_id = models.IntegerField(default = IntervalEntry.objects.count(), editable=False)
	distance = models.FloatField()
	time = models.DurationField()
	def __str__(self):
		rtnstring = "%s miles in %s" % (str(self.distance), str(self.time))
		if(self.distance == 1):
			rtnstring = rtnstring.replace("miles", "mile")
		return rtnstring