# README #

### What is this repository for? ###

* This repository contains code for a website that tracks daily running workouts.  A user inputs information
	about runs through using the appropriate form, then statistics are displayed in a variety of formats on 
	the main page.
* Version 1.0 (Prototype)

### How do I get set up? ###

* Download [Django](https://www.djangoproject.com/download/) or for [Windows](https://docs.djangoproject.com/en/1.11/howto/windows/)
* Copy code to a local directory
* Use command line to navigate to the logsite directory
* To see site: run 'python manage.py runserver' and then go to [IP address](http://127.0.0.1:8000/main/)
* To run tests: run 'python manage.py test newapp'

### Who do I talk to? ###

* Jessica Gaines
* jlgaines2@gmail.com"# hello-world" 
